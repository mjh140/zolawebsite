#### Personal Website: https://zolawebsite-mjh140-84221a449a6f2f4baac19b4d09c0cf8992f2fddab7c3.gitlab.io/

## Zola Theme


This website was built using Zola. Zola is static site generator written in Rust - a programming language known for its performance, memory efficiency, and safety<!-- more -->.
This website is based on the a <a href="https://github.com/dave-tucker/zola-clean-blog" target="_blank">blog template</a> that can be found on <a href="https://www.getzola.org/" target="_blank">Zola's homepage</a>. 


## Website Features


- Paginated Home/Categories/Tag Pages
- Customizable Menu
- Customizable Social Links

## Template


<a href="https://www.getzola.org/themes/clean-blog/" target="_blank">
    <img src="/img/zola_demo.png" class="gallery" alt="Zola Template Demo" />
</a>