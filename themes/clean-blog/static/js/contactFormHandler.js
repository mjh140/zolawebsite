document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('contactForm').addEventListener('submit', function(e) {
        e.preventDefault(); // Prevent the default form submission

        var formData = {
            name: document.getElementById('name').value,
            email: document.getElementById('email').value,
            message: document.getElementById('message').value
        };

        fetch('https://d0g0cn9vl8.execute-api.us-east-2.amazonaws.com/default/contactmerust', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData)
        })
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {
            console.log('Success:', data);
            var submissionModal = new bootstrap.Modal(document.getElementById('submissionModal'));
            submissionModal.show();
            document.getElementById('contactForm').reset()
        })
        .catch(function(error) {
            console.error('Error:', error);
            alert('There was a problem with your submission. Please try again later.');
        });
    });

});
